const express = require("express");
const bodyParser = require("body-parser");
const mysql = require("mysql");
const cors = require("cors");

const app = express();
const port = 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

const connection = mysql.createConnection({
  host: "localhost",
  user: "root",
  password: "",
  database: "front",
});

connection.connect((err) => {
  if (err) throw err;
  console.log("Connecté à la base de données MySQL");
});

app.post("/login", (req, res) => {
  const { email, mdp } = req.body;

  const sqlEtudiant = `SELECT * FROM etudiant WHERE email = ? AND mdp = ?`;
  const sqlEnseignant = `SELECT * FROM enseignant WHERE email = ? AND mdp = ?`;

  connection.query(sqlEtudiant, [email, mdp], (errEtudiant, resultEtudiant) => {
    if (errEtudiant) {
      res.status(500).json({ message: "Erreur lors de la connexion" });
    } else if (resultEtudiant.length > 0) {
      console.log("Données de l'étudiant:", resultEtudiant[0]);
      res.status(200).json({
        message: "Connexion réussie en tant qu'étudiant",
        role: "etudiant",
        userData: resultEtudiant[0], // Envoyer les données de l'utilisateur
      });
    } else {
      connection.query(
        sqlEnseignant,
        [email, mdp],
        (errEnseignant, resultEnseignant) => {
          if (errEnseignant) {
            res.status(500).json({ message: "Erreur lors de la connexion" });
          } else if (resultEnseignant.length > 0) {
            console.log("Données de l'enseignant:", resultEnseignant[0]);
            res.status(200).json({
              message: "Connexion réussie en tant qu'enseignant",
              role: "enseignant",
              userData: resultEnseignant[0], // Envoyer les données de l'utilisateur
            });
          } else {
            res
              .status(401)
              .json({ message: "Email ou mot de passe incorrect" });
          }
        }
      );
    }
  });
});

app.get("/check", (req, res) => {
  res.status(200).send("OK");
});
app.get("/enseignant/:idEnseignant/cours", (req, res) => {
  const idEnseignant = req.params.idEnseignant;

  const sql = `SELECT * FROM Cours WHERE idEnseignant = ?`;

  connection.query(sql, [idEnseignant], (err, result) => {
    if (err) {
      console.log(
        "Erreur lors de la récupération des cours de l'enseignant:",
        err
      );
      res.status(500).json({
        message: "Erreur lors de la récupération des cours de l'enseignant",
      });
    } else {
      res.status(200).json(result);
    }
  });
});

app.get("/cours", (req, res) => {
  const departement = req.query.departement; // Récupérer le département depuis la requête

  let sql = `SELECT * FROM Cours`;

  if (departement) {
    sql += ` WHERE idCours IN (
      SELECT idCours FROM Cours INNER JOIN Etudiant ON Cours.idEtudiant = Etudiant.idEtudiant
      WHERE Etudiant.departement = ?)`;
  }

  connection.query(sql, [departement], (err, result) => {
    if (err) {
      console.log("Erreur lors de la récupération des cours:", err);
      res
        .status(500)
        .json({ message: "Erreur lors de la récupération des cours" });
    } else {
      res.status(200).json(result);
    }
  });
});

app.listen(port, () => {
  console.log(`Serveur en écoute sur le port ${port}`);
});
